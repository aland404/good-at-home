import * as React from 'react'
import { Route, Switch } from 'react-router-dom'
import AwesomeInterface from './components/AwesomeInterface'

const Routes: React.FunctionComponent = () => (
  <div>
    <Switch>
      <Route exact path="/" component={AwesomeInterface} />
      <Route
        path="/settings"
        component={() => <div>Page des paramètres</div>}
      />
      <Route path="/charts" component={() => <div>Page des graphiques</div>} />
      <Route component={() => <div>Non trouvé</div>} />
    </Switch>
  </div>
)

export default Routes
