import React from 'react'
import './App.scss'
import { ConnectedRouter } from 'connected-react-router'
import { History } from 'history'
import PageAccueil from './layouts/page-accueil/PageAccueil'

// Any additional component props go here.
interface AppProps {
  history: History;
}

const App: React.FC<AppProps> = ({ history }) => {
  return (
    <div className="App">
      <ConnectedRouter history={history}>
        <PageAccueil />
      </ConnectedRouter>
    </div>
  )
}

export default App
