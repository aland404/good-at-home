import SocketIOClient from 'socket.io-client'

export interface AwesomeInterface {}

export interface AwesomeInterfaceProps {}

export interface AwesomeInterfaceState {
  socket: SocketIOClient.Socket | undefined;
  data: Array<Object>;
}
