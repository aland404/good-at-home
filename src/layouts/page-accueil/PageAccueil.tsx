import React from 'react'
import { Layout, Menu, Alert } from 'antd'
import { Link } from 'react-router-dom'

import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  AreaChartOutlined,
  PoweroffOutlined,
} from '@ant-design/icons'
import { connect } from 'react-redux'
import logo from '../../assets/house.png'
import { ApplicationState } from '../../store'
import './PageAccueil.scss'
import { PageAccueilState } from './PageAccueilInterface'
import Routes from '../../routes'

const { Header, Sider, Content } = Layout

// Separate state props + dispatch props to their own interfaces.
interface PropsFromState {
  attemptsConnectionCount: number;
  socketIsConnected: boolean;
}
// Combine both state + dispatch props - as well as any props we want to pass - in a union type.
type AllProps = PropsFromState

class PageAccueil extends React.PureComponent<AllProps, PageAccueilState> {
  constructor(props: AllProps) {
    super(props)
    this.state = {
      collapsed: false,
    }
  }

  private toggle = () => {
    this.setState((prevState) => {
      return {
        collapsed: !prevState.collapsed,
      }
    })
  }

  render() {
    const { attemptsConnectionCount, socketIsConnected } = this.props
    const { collapsed } = this.state

    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo">
            <img
              src={logo}
              alt="Logo"
              style={{ height: '40px', width: '40px' }}
            />
          </div>
          <Menu theme="dark" defaultSelectedKeys={[window.location.pathname]}>
            <Menu.Item key="/">
              <Link to="/">
                <PoweroffOutlined />
                <span>Contrôles</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/charts">
              <Link to="/charts">
                <AreaChartOutlined />
                <span>Graphiques</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/settings">
              <Link to="/settings">
                <UserOutlined />
                <span>Paramétrage</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{ textAlign: 'left', padding: 0 }}
          >
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              { className: 'trigger', onClick: this.toggle }
            )}
            {socketIsConnected && (
              <Alert
                message="Connecté au serveur avec succès!"
                type="success"
                closable
              />
            )}
            {attemptsConnectionCount >= 3 ? (
              <Alert
                message="Problème de connexion socket, trop de tentatives infructueuses..."
                type="error"
                closable
              />
            ) : (
              <Alert
                message={`En cours de connexion socket... Nombre de tentatives:${attemptsConnectionCount}`}
                type="info"
                closable
              />
            )}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 500,
            }}
          >
            <Routes />
          </Content>
        </Layout>
      </Layout>
    )
  }
}

// It's usually good practice to only include one context at a time in a connected component.
// Although if necessary, you can always include multiple contexts. Just make sure to
// separate them from each other to prevent prop conflicts.
const mapStateToProps = ({ socket }: ApplicationState) => ({
  attemptsConnectionCount: socket.attemptsConnectionCount,
  socketIsConnected: socket.socketIsConnected,
})

// Now let's connect our component!
// With redux v4's improved typings, we can finally omit generics here.
export default connect(mapStateToProps)(PageAccueil)
