import * as React from 'react'
import { connect as connectRedux } from 'react-redux'
import { Card, Row, Col } from 'antd'
import { connect } from 'socket.io-client'
import { Chart, Axis, Line, Area } from 'bizcharts'
import Switch from './switch/Switch'
import Thermometer from './thermometer/Thermometer'
import {
  AwesomeInterfaceProps,
  AwesomeInterfaceState,
} from '../interfaces/AwesomeInterface'
import './AwesomeInterface.scss'
import {
  addAttempConnection,
  resetAttempConnection,
  setIsSocketConnected,
} from '../store/socket/actions'
import { ApplicationState } from '../store'

const ENDPOINT = 'http://localhost:3030'

// Separate state props + dispatch props to their own interfaces.
interface PropsFromState {
  attemptsConnectionCount: number;
}
// We can use `typeof` here to map our dispatch types to the props, like so.
interface PropsFromDispatch {
  addAttempConnection: typeof addAttempConnection;
  resetAttempConnection: typeof resetAttempConnection;
  setIsSocketConnected: typeof setIsSocketConnected;
}
// Combine both state + dispatch props - as well as any props we want to pass - in a union type.
type AllProps = AwesomeInterfaceProps & PropsFromState & PropsFromDispatch

class AwesomeInterface extends React.PureComponent<
AllProps,
AwesomeInterfaceState
> {
  constructor(props: AllProps) {
    super(props)
    this.state = {
      socket: undefined,
      data: [],
    }
  }

  async componentDidMount() {
    const socket = connect(ENDPOINT)
    this.setState({ socket })

    this.props.addAttempConnection()
    console.info('Tentative de connexion socket')
    socket?.on('connect_error', () => {
      this.props.setIsSocketConnected(false)
      if (this.props.attemptsConnectionCount >= 3) {
        this.state.socket?.close()
        console.error(
          'Fermeture de la connexion, trop de tentatives infructueuses'
        )
      } else {
        this.props.addAttempConnection()
        console.info('connect_error, nouvelle tentative de connexion socket')
      }
    })
    socket?.on('connect', () => {
      this.props.resetAttempConnection()
      this.props.setIsSocketConnected(true)
    })

    this.state.socket?.on('température', (temp: number) => {
      this.setState((prevState) => {
        return {
          data: [
            ...prevState.data,
            { temperature: temp, number: prevState.data.length },
          ],
        }
      })
    })
  }

  render() {
    const { socket, data } = this.state
    const cols = {
      temperature: {
        min: 0,
        max: 35,
      },
    }
    return (
      <Row>
        <Col xs={24} sm={24} md={12}>
          <Card title="Gestion de la prise B" className="card">
            <Switch
              socket={socket}
              codePrise="B"
              codeTurnOn="4460881"
              codeTurnOff="4460884"
            />
          </Card>
        </Col>
        <Col sm={24} md={12}>
          <Card title="Température" className="card">
            <Thermometer socket={socket} />
          </Card>
        </Col>
        <Col sm={24} md={12}>
          <Card title="Gestion de la prise C" className="card">
            <Switch
              socket={socket}
              codePrise="C"
              codeTurnOn="4461649"
              codeTurnOff="4461652"
            />
          </Card>
        </Col>
        <Col sm={24} md={12}>
          <Card title="Gestion de la prise D" className="card">
            <Switch
              socket={socket}
              codePrise="D"
              codeTurnOn="4461841"
              codeTurnOff="4461844"
            />
          </Card>
        </Col>
        <Col sm={24}>
          <Card
            title="Evolution de la température"
            className="card temperature"
          >
            <Chart
              height={100}
              style={{ width: '100%' }}
              data={data}
              forceFit
              scale={cols}
            >
              <Axis
                name="temperature"
                label={{ formatter: (val: Object) => `${val}°C` }}
              />
              <Line position="number*temperature" size={2} color="city" />
              <Area position="number*temperature" />
            </Chart>
          </Card>
        </Col>
      </Row>
    )
  }
}

// It's usually good practice to only include one context at a time in a connected component.
// Although if necessary, you can always include multiple contexts. Just make sure to
// separate them from each other to prevent prop conflicts.
const mapStateToProps = ({ socket }: ApplicationState) => ({
  attemptsConnectionCount: socket.attemptsConnectionCount,
})
// mapDispatchToProps is especially useful for constraining our actions to the connected component.
// You can access these via `this.props`.
const mapDispatchToProps = {
  addAttempConnection,
  resetAttempConnection,
  setIsSocketConnected,
}

// Now let's connect our component!
// With redux v4's improved typings, we can finally omit generics here.
export default connectRedux(
  mapStateToProps,
  mapDispatchToProps
)(AwesomeInterface)
