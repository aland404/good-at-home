import SocketIOClient from 'socket.io-client'

export interface ThermometerProps {
  socket: SocketIOClient.Socket | undefined;
}

export interface ThermometerState {
  hover: boolean;
  disabled: boolean;
  temperature: string | undefined;
}
