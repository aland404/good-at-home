import React from 'react'
import { Button, Col, Row } from 'antd'
import { ThermometerProps, ThermometerState } from './ThermometerInterfaces'
import './Thermometer.scss'
import thermometer from '../../assets/thermometer.png'

class Thermometer extends React.PureComponent<
ThermometerProps,
ThermometerState
> {
  constructor(props: ThermometerProps) {
    super(props)
    this.state = {
      hover: false,
      disabled: false,
      temperature: undefined,
    }
  }

  async componentDidMount() {
    this.props.socket?.on('température', (data: number) => {
      this.setState({ temperature: data.toFixed(1) })
    })
  }

  toggleHover = () => {
    this.setState((prevState) => {
      return { hover: !prevState.hover }
    })
  }

  openGraphWindow = () => {
    console.log('Ouverture du graph')
  }

  render() {
    const { temperature } = this.state
    return (
      <Row>
        <Col xs={24} sm={14} style={{ fontSize: '30px', textAlign: 'right' }}>
          {temperature}
        </Col>
        <Col xs={24} sm={8}>
          <Button
            type="link"
            onClick={this.openGraphWindow}
            onMouseEnter={this.toggleHover}
            onMouseLeave={this.toggleHover}
            className="interrupteur"
            disabled={this.state.disabled}
            icon={
              <img
                src={thermometer}
                alt="thermometer"
                style={{ height: '50px', width: '50px' }}
              />
            }
          />
        </Col>
      </Row>
    )
  }
}

export default Thermometer
