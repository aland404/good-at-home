import SocketIOClient from 'socket.io-client'

export interface SwitchProps {
  socket: SocketIOClient.Socket | undefined;
  codePrise: string;
  codeTurnOn: string;
  codeTurnOff: string;
}

export interface SwitchState {
  hover: boolean;
  disabled: boolean;
  prise: '0' | '1';
}
