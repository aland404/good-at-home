import React from 'react'
import { Button } from 'antd'
import { PoweroffOutlined } from '@ant-design/icons'
import { SwitchProps, SwitchState } from './SwitchInterfaces'
import './Switch.scss'

class Switch extends React.PureComponent<SwitchProps, SwitchState> {
  constructor(props: SwitchProps) {
    super(props)
    this.state = {
      hover: false,
      prise: '0',
      disabled: false,
    }
  }

  async componentDidMount() {
    this.props.socket?.on('message', (data: string) => {
      if (data === this.codeTurnOn()) {
        this.setState({ prise: '1' })
      } else if (data === this.codeTurnOff()) {
        this.setState({ prise: '0' })
      } else {
        return
      }
      this.setState({ disabled: false })
    })
  }

  toggleHover = () => {
    this.setState((prevState) => {
      return { hover: !prevState.hover }
    })
  }

  toggleSwitch = () => {
    this.props.socket?.emit(
      'message',
      this.state.prise === '0' ? this.codeToTurnOn() : this.codeToTurnOff()
    )
    this.setState({ disabled: true })
  }

  codeToTurnOn() {
    return `${this.codeTurnOn()}${this.props.codeTurnOn}`
  }

  codeToTurnOff() {
    return `${this.codeTurnOff()}${this.props.codeTurnOff}`
  }

  codeTurnOn() {
    return `1${this.props.codePrise}`
  }

  codeTurnOff() {
    return `0${this.props.codePrise}`
  }

  render() {
    return (
      <Button
        type="link"
        onClick={this.toggleSwitch}
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        className="interrupteur"
        disabled={this.state.disabled}
        icon={
          <PoweroffOutlined
            className={this.state.prise === '1' ? 'on' : 'off'}
          />
        }
      />
    )
  }
}

export default Switch
