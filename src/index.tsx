import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'

import './index.css'
import 'antd/dist/antd.css'
import { Provider } from 'react-redux'
import App from './App'
import * as serviceWorker from './serviceWorker'

import configureStore from './configueStore'

// We use hash history because this example is going to be hosted statically.
// Normally you would use browser history.
const history = createBrowserHistory()

const initialState = window.INITIAL_REDUX_STATE
const store = configureStore(history, initialState)

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
