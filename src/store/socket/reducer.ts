import { Reducer } from 'redux'
import { SocketState, SocketActionTypes } from './types'

// Type-safe initialState!
export const initialState: SocketState = {
  attemptsConnectionCount: 0,
  socketIsConnected: false,
}

const reducer: Reducer<SocketState> = (state = initialState, action) => {
  switch (action.type) {
    case SocketActionTypes.ADD_ATTEMPS_CONNECTION: {
      return {
        ...state,
        attemptsConnectionCount: state.attemptsConnectionCount + 1,
      }
    }
    case SocketActionTypes.RESET_ATTEMPS_CONNECTION: {
      return { ...state, attemptsConnectionCount: 0 }
    }
    case SocketActionTypes.SET_IS_SOCKET_CONNECTED: {
      return { ...state, socketIsConnected: action.payload }
    }
    default: {
      return state
    }
  }
}

// Instead of using default export, we use named exports. That way we can group these exports
// inside the `index.js` folder.
export { reducer as socketReducer }
