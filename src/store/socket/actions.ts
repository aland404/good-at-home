import { action } from 'typesafe-actions'
import { SocketActionTypes } from './types'

// Here we use the `action` helper function provided by `typesafe-actions`.
// This library provides really useful helpers for writing Redux actions in a type-safe manner.
// For more info: https://github.com/piotrwitek/typesafe-actions
export const addAttempConnection = () =>
  action(SocketActionTypes.ADD_ATTEMPS_CONNECTION)
export const resetAttempConnection = () =>
  action(SocketActionTypes.RESET_ATTEMPS_CONNECTION)
export const setIsSocketConnected = (isSocketConnected: boolean) =>
  action(SocketActionTypes.SET_IS_SOCKET_CONNECTED, isSocketConnected)
