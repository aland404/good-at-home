// Declare state types with `readonly` modifier to get compile time immutability.
export interface SocketState {
  readonly attemptsConnectionCount: number;
  readonly socketIsConnected: boolean;
}

// Use `enum`s for better autocompletion of action type names
// `@@context/ACTION_TYPE` convention, to follow the convention of Redux's `@@INIT` action.
export enum SocketActionTypes {
  ADD_ATTEMPS_CONNECTION = '@@socket/ADD_ATTEMPS_CONNECTION',
  RESET_ATTEMPS_CONNECTION = '@@socket/RESET_ATTEMPS_CONNECTION',
  SET_IS_SOCKET_CONNECTED = '@@socket/SET_IS_SOCKET_CONNECTED',
}
