import { combineReducers } from 'redux'
import { connectRouter, RouterState } from 'connected-react-router'
import { History } from 'history'
import { SocketState, socketReducer } from './socket'

// The top-level state object
export interface ApplicationState {
  socket: SocketState;
  router: RouterState;
}

// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
export const createRootReducer = (history: History) =>
  combineReducers({
    socket: socketReducer,
    router: connectRouter(history),
  })
